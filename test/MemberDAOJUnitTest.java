import DAO.memberDao;
import Exceptions.DaoException;
import SYSTEM_Object.Member;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;

public class MemberDAOJUnitTest {
    
    public MemberDAOJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     public void testFindAllUsers() throws DaoException {  
        System.out.println("findAllUsers():");
        memberDao dao = new memberDao();
        List<Member> users = new ArrayList<Member>();
        List<Member> users2 = new ArrayList<Member>();
        Member a = new Member(1,"LEE","122");
        Member b = new Member(3,"lol","pp");
        Member c = new Member(4,"ewwg","334");
        Member d = new Member(7,"kkfnn","99844");
        Member e = new Member(8,"LKKSJ","2258");
        Member f = new Member(9,"OOPPPS","3394");
        Member g = new Member(10,"LALSPOSW","SA3232");
        Member h = new Member(11,"DSVS","3RFEFE");
        Member i = new Member(12,"PPLLLSSS","1122");
        Member j = new Member(14,"kkdddd","123");
        users.add(a);users.add(b);users.add(c);users.add(d);
        users.add(e);users.add(f);users.add(g);users.add(h);
        users.add(i);users.add(j);
        users2 = dao.findAllUsers();
        assertEquals("testFindAllUsers()",users, users2);
    }
}
