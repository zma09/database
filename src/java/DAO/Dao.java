package DAO;

import Exceptions.DaoException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Dao 
{
    private DataSource datasource;
    
    // Add in constructor that takes in the source of database connections and store it
    public Dao(DataSource myDataSource)
    {
        this.datasource = myDataSource;
    }
    
    // Constructor for use by pooled connections
    public Dao()
    {
        Connection con = null;
        String DATASOURCE_CONTEXT = "jdbc/xujiba";
        try {
            Context initialContext = new InitialContext();
            DataSource ds = (DataSource)initialContext.lookup("java:comp/env/" + DATASOURCE_CONTEXT);
            if(ds != null){
                datasource = ds;
            }
            else{
		System.out.println(("Failed to lookup datasource."));
            }
        }catch (NamingException ex ){
            System.out.println("Cannot get connection: " + ex);
        }
    }
    
    // Original version of getConnection
    /*
    public Connection getConnection() throws DaoException 
    {

        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/user_database";
        String username = "root";
        String password = "";
        Connection con = null;
        
        try 
        {
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
        } 
        catch (ClassNotFoundException ex1) 
        {
            System.out.println("Failed to find driver class " + ex1.getMessage());
            System.exit(1);
        } 
        catch (SQLException ex2) 
        {
            System.out.println("Connection failed " + ex2.getMessage());
            System.exit(2);
        }
        return con;
    }
*/
    // Pooled version of getConnection
    public Connection getConnection() throws DaoException
    {
        Connection conn = null;
        try{
            if (datasource != null) {
                conn = datasource.getConnection();
            }else {
                System.out.println(("Failed to lookup datasource."));
            }
        }catch (SQLException ex2){
            System.out.println("Connection failed " + ex2.getMessage());
            System.exit(2); // Abnormal termination
        }
        return conn;
    }

    public void freeConnection(Connection con) throws DaoException 
    {
        try 
        {
            if (con != null) 
            {
                con.close();
                con = null;
            }
        } 
        catch (SQLException e) 
        {
            System.out.println("Failed to free connection: " + e.getMessage());
            System.exit(1);
        }
    }   
}