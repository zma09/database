package DAOInterface;

import Exceptions.DaoException;
import SYSTEM_Object.Admin;

public interface adminDaoInterface {
    public Admin login(String AdminUsername,String AdminPassword) throws DaoException;
    public int register(String AdminUsername,String AdminPassword) throws DaoException;  
}
