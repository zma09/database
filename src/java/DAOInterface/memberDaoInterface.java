package DAOInterface;

import Exceptions.DaoException;
import SYSTEM_Object.Member;

public interface memberDaoInterface {
    public Member login(String MemberUsername,String MemberPassword) throws DaoException;
    public int register(String MemberUsername,String MemberPassword) throws DaoException;
}
