
package Command;

import Servis.Userservice;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class listAllsCommand implements Command{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
                        String forwardToJsp = null;
                       
                        HttpSession session  = request.getSession();
                        
                        Userservice userService = new Userservice();
                        ArrayList<String> arc = new ArrayList<String>();
                        arc = userService.listAll();
                        session.setAttribute("check","false");
                        session.setAttribute("listss", null);
                        session.setAttribute("list", arc);
                        
                        forwardToJsp = "/home.jsp";
                        return  forwardToJsp;
    }
    
}
