
package Command;


public class CommandFactory {
    private static CommandFactory factory = null;
    
    private CommandFactory() {
    }
	
    public static synchronized CommandFactory getInstance()
    {
        if(factory == null)
        {
            factory = new CommandFactory();
        }
        return factory;
    }
    
    public synchronized Command createCommand(String commandStr) 
    {
    	Command command = null;
    	
	//Instantiate the required Command object...
    	if (commandStr.equals("Log In")) {
    		command = new LoginCommand();
    	}
        if (commandStr.equals("Signup")){
                command = new RegisterCommand();
        }
        if (commandStr.equals("post")){
                command = new PostCommand();
        }
        if (commandStr.equals("logout")){
                command = new logoutCommand();
        }
        if (commandStr.equals("listAllUser")){
                command = new listAllUserCommand();
        }
        if (commandStr.equals("listAllPost")){
                command = new listAllsCommand();
        }
        
        if (commandStr.equals("arccet")){
                command = new deleteCommand();
        }
        
         if (commandStr.equals("delete")){
                command = new delete2Command();
        }
    	//Return the instantiated Command object to the calling code...

    	return command;// may be null
    }    
}
