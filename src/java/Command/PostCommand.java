package Command;

import SYSTEM_Object.Member;
import Servis.Userservice;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PostCommand implements Command{

    
    public String execute(HttpServletRequest request, HttpServletResponse response) {
                        
        String forwardToJsp= "/home.jsp";                 
        String text = request.getParameter("post");
        HttpSession session  = request.getSession();           
                       
        if(!text.equals("")){
            Member temp123 = (Member)session.getAttribute("us");
  
            Userservice userService = new Userservice();
                      
            int temp =userService.postService(text,temp123);
                      
            ArrayList<String> arc = new ArrayList<String>();
                         
            arc = userService.listAll();
                         
            session.setAttribute("list", arc);
    
            forwardToJsp = "/home.jsp";                                      
        }else{
            session.setAttribute("emptyError", "Diary can't be empty.!");
        }
                     
        return  forwardToJsp;
    }
    
}
