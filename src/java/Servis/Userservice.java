package Servis;

import DAO.memberDao;
import DAO.ssDao;
import Exceptions.DaoException;
import SYSTEM_Object.Member;
import java.util.ArrayList;
import java.util.List;

public class Userservice {
     public Member login(String username, String password)
    {
        memberDao dao = new memberDao();
        Member u = null;
        try 
        {
            u = dao.login(username, password);
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return u;
    }
     
    public Member register(String username, String password)
    {
        memberDao dao = new memberDao();
        int in;
        Member u = null;
        try 
        {
            in = dao.register(username, password);
            u = dao.login(username, password);
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return u;
    }
    
    public int postService(String text, Member x)
    {
        ssDao dao = new ssDao();
        
        try 
        {
           dao.addsss(text, x);
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return 1;
        
    }
    
    public ArrayList<String> listAll()
    {
        ssDao dao = new ssDao();
         ArrayList<String> arc = new ArrayList<String>();
        
        try 
        {
            arc = dao.listAll();
            
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return arc;
    }
    
    public List<Member> listAllUser()
    { 
        memberDao dao = new memberDao();
         List<Member> arc = null;
        try 
        {
        
         arc = dao.findAllUsers();
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return arc;
    }
    
    public int deleteUser(String username)
    { 
        ssDao dao = new ssDao();
        int a = 0;
        try 
        {
            a  = dao.delete(username);
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return a;
    }
    
}
