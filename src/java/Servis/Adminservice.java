package Servis;

import DAO.adminDao;
import DAO.ssDao;
import Exceptions.DaoException;
import SYSTEM_Object.Admin;
import java.util.ArrayList;

public class Adminservice {
     public Admin login(String username, String password)
    {
        adminDao dao = new adminDao();
        Admin u = null;
        try 
        {
            u = dao.login(username, password);
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return u;
    }
     
 
    public ArrayList<String> listAll()
    {
        ssDao dao = new ssDao();
         ArrayList<String> arc = new ArrayList<String>();
        
        try 
        {
            arc = dao.listAll();
            
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return arc;
    }
    
    
}
