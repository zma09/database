<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*" %>

<c:set var="language" 
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session" />

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />

<!DOCTYPE html>
<html>
    <title>Student info system loggin page</title>
   <link rel="stylesheet" href="web_source/lks.css" type="text/css">
   <script src="web_source/logging.js" type="text/javascript"></script>
    <body>
        
        <form>
            <select id="language" name="language" onchange="submit()">
                <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                <option value="nl" ${language == 'nl' ? 'selected' : ''}>Nederlands</option>
                <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
            </select>
        </form>
       
            <form id="login" action="UserActionServlet" method = "post"> 

    <h1>Log In</h1>
<%
         String result = (String)(request.getSession().getAttribute("Error"));  
         if(result==null){
             result = "";   
         }
        %>
        <div id="por"><h1><%=result%></h1></div>
    <fieldset id="inputs">
        
        <label for="username"><fmt:message key="login.label.username" />:</label>
        <input id="username" name="username" type="text" placeholder="username" autofocus required>
        <label for="password"><fmt:message key="login.label.password" />:</label>
        <input id="password" name="password" type="password" placeholder="password" required>

    </fieldset>

    <fieldset id="actions">

         <input type="hidden" name="action" value="Log In" />
        
         <input type="submit" id="submit" value="Log in" >

        <a href="#">Forgot your password?</a><a href="register.html">Register</a>

    </fieldset>

</form>
    </body>
</html>
